# Wordlist - Termos mais comuns utilizados e suas traduções

## Equipe brasileira de tradução

### Apresentação

Esta é uma tabela colaborativa baseada na wordlist do sistema DDTP para tradução
de descrição de pacotes. O objetivo é diminuir as dúvidas que os tradutores tem
em relação a tradução de alguns termos e também manter um padrão de tradução.

### Modo de uso e recomendações

Esta é uma tabela colaborativa, todos que possuem login no sistema salsa podem
contribuir e recomendamos fortemente que todos os membros da equipe realizem o
cadastro para tal. Sugerimos que observem o padrão utilizado na construção da
tabela e sigam o mesmo. Estamos abertos a sugestões de mudança, basta enviar um
e-mail para a lista propondo e iremos debater. A tabela pode ser usada para
qualquer tipo de tradução, páginas da web, wiki, descrição de pacotes, pacotes
e etc.

Caso tenha dúvida em relação a algum termo e sua tradução, envie um e-mail para
a lista e peça a opinião dos demais membros da equipe para que possamos juntos
definir qual a melhor opção.

Original   | Tradução
--------- | ------
applet  | miniaplicativo
artwork | ilustração
backend | (Sugestão: mecanismo)
binding | vínculo
bookmark | favorito ou no GNOME marcador
boot | inicialização
bug tracking system | sistema de acompanhamento de bugs
built in | embutido
character | caractere
character set | conjunto de caracteres
chat  | Sugestão: bate-papo
code folding | dobrar blocos de código
consistent | coerente
copyright | copyright
cross-platform | interplataforma
custom | personalizar
custom debian distribution | Distribuição Debian Personalizada
daemon | daemon
decrypt | descriptografar
delete | excluir
deprecate | tornar obsoleto
deprecated | obsoleto
desktop | área de trabalho
desktop environment | ambiente de área de trabalho
display | exibir
dock | dock, doca
download | baixar
drag'n'drop | arrastar e soltar
drop-down | suspenso(a)
drop-down list | lista suspensa
dummy package | pacote fictício ("dummy")
encrypt | criptografar
feature | recurso
feed | feed
find | localizar
framework | infraestrutura
front-end | interface
gathering | coleta
hardware | hardware
homepage |Página web
infrastructure | infraestrutura
more information | mais informações
multiplatform | multiplataforma
multi-section | multisessão
on the fly | Sugestão: sob demanda
online | on-line
onthefly | Sugestão: sob demanda
parse | analisar
parser | analisador
performance | desempenho
pin | APT pinning: alfinetar
plug-in | plug-in, extensão, complemento
pop-up | janela instantânea
preseed | pré-configuração
purge | expurgar
raw | não processado
run | executar
runtime | execução
search | pesquisar
shell | shell, camada (biologia/química)
show | mostrar
site | site
software | software
spin box | seletor numérico
standalone | autônomo
switcher | Sugestão: alternador
syntax highlight | realce de sintaxe
syntax highlighting | realce de sintaxe
system tray | bandeja de sistema
systray | bandeja de sistema
text folding | dobrar blocos de texto
toolchain | Sugestão: cadeia base de ferramentas ("toolchain")
toolkit | kit de ferramentas
tweak | Sugestão: ajustar, refinar
tweaker | Sugestão: refinador
upstream | Sugestão: autor original
web page | página web
web site | site web
webpage | página web
website | site web
x window system | x window system
